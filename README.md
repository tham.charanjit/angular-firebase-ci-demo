# AngularFirebaseCiDemo

Demo of how to automatically build, test and deploy an *Angular 6* application to *Firebase* with *GitLab CI/CD*. 

More information can be found here: 

* [CI/CD with Angular 6 & Firebase & GitLab](https://medium.com/@kniklas/ci-cd-with-angular-6-firebase-gitlab-5118ad469e4d)

## Build for Production

Run `npm run build-prod` to build the project for productive usage. The build artifacts will be stored in the `dist/` directory.

## Test

Run `npm run test-ci` to execute the unit tests via [Karma](https://karma-runner.github.io).
Run `npm run e2e-ci` to execute the e2e tests via [Protractor](https://www.protractortest.org).

## Deployment

Run `npm run deploy` to deploy to [Firebase](https://firebase.google.com/).

## Skip CI Build, Test, Deployment

Include anywhere in commit message the keywork `skip-ci`.